﻿using System;
using System.Windows.Input;

namespace Keno.ViewModel.Commands
{
    public class CommonCommand : ICommand
    {
        #region Variables 

        private ICommandOnExecute _execute;
        private ICommandOnCanExecute _canExecute;

        #endregion
        #region Delegates

        public delegate void ICommandOnExecute(object parameter);
        public delegate bool ICommandOnCanExecute(object parameter);

        #endregion

        #region Constructor

        public CommonCommand(ICommandOnExecute onExecuteMethod, ICommandOnCanExecute onCanExecuteMethod)
        {
            _execute = onExecuteMethod;
            _canExecute = onCanExecuteMethod;
        }

        public CommonCommand(ICommandOnExecute onExecuteMethod)
        {
            _execute = onExecuteMethod;
            _canExecute = null;
        }

        #endregion

        #region ICommand Members

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if (_canExecute != null)
                return _canExecute.Invoke(parameter);
            else
                return true;
        }

        public void Execute(object parameter)
        {
            _execute?.Invoke(parameter);
        }

        #endregion
    }
}
