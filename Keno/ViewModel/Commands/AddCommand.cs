﻿using Keno.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Keno.ViewModel.Commands
{
    public class AddCommand : ICommand
    {
        #region ICommand Implementation 

        public bool CanExecute(object parameter)
        {
            // If the list is not over the Spot Value
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            
        }

        #endregion
    }
}
