﻿using Keno.Model;
using Keno.UserControls;
using Keno.ViewModel.Commands;
using System.Collections.ObjectModel;

namespace Keno.ViewModel
{
    public class GameViewModel
    {
        #region Variables

        private readonly ObservableCollection<int> _pickNumberList;
        private readonly ObservableCollection<int> _spotNumberList;
        private ScoreItemList _scoring;

        private int _selectedNumber;
        private int _selectedUserNumber;
        private bool _isPlayedDirty = false;

        private Game _game;

        #endregion
        #region Command Variables

        private CommonCommand _addCommand;
        private CommonCommand _removeCommand;
        private CommonCommand _playCommand;
        private CommonCommand _restartCommand;

        #endregion

        #region Properties

        public int SelectedNumber
        {
            get
            { return _selectedNumber; }
            set
            { _selectedNumber = value; }
        }

        public int SelectedUserNumber
        {
            get
            { return _selectedUserNumber; }
            set
            { _selectedUserNumber = value; }
        }

        public bool IsPlayedDirty
        {
            get { return _isPlayedDirty; }
            set { _isPlayedDirty = value; }
        }

        public Game Game
        {
            get { return _game; }
        }

        #endregion
        #region ObservableCollection Properties

        public ObservableCollection<int> ToPickNumbers
        {
            get
            {
                return _pickNumberList;
            }
        }

        public ObservableCollection<int> SpotNumbers
        {
            get
            {
                return _spotNumberList;
            }
        }

        #endregion
        #region Command Properties

        public CommonCommand AddCommand
        {
            get
            {
                return _addCommand;
            }
            set
            {
                _addCommand = value;
            }
        }

        public CommonCommand RemoveCommand
        {
            get
            {
                return _removeCommand;
            }
            set
            {
                _removeCommand = value;
            }
        }

        public CommonCommand PlayCommand
        {
            get
            {
                return _playCommand;
            }
            set
            {
                _playCommand = value;
            }
        }

        public CommonCommand RestartCommand
        {
            get
            {
                return _restartCommand;
            }
            set
            {
                _restartCommand = value;
            }
        }

        #endregion

        #region Constructor

        public GameViewModel()
        {
            _game = new Game();

            _pickNumberList = GameLists.PossibleNumbers();
            _spotNumberList = GameLists.PossileSpots();
            _scoring = new ScoreItemList(SpotNumbers.Count);

            _selectedNumber = _pickNumberList[0];
            _game.Spot = _spotNumberList[0];

            _addCommand = new CommonCommand(ExecuteAddCommand, CanExecuteAddCommand);
            _removeCommand = new CommonCommand(ExecuteRemoveCommand, CanExecuteRemoveCommand);
            _playCommand = new CommonCommand(ExecutePlayCommand, CanExecutePlayCommand);
            _restartCommand = new CommonCommand(ExecuteRestartCommand);
        }

        #endregion // Constructor

        #region Add Command Methods

        private bool CanExecuteAddCommand(object parameter)
        {
            return Game.UserNumbers.Count < Game.Spot;
        }

        private void ExecuteAddCommand(object parameter)
        {
            if (!Game.UserNumbers.Contains(SelectedNumber))
                Game.UserNumbers.Add(SelectedNumber);
        }

        #endregion
        #region Remove Command Methods

        private bool CanExecuteRemoveCommand(object parameter)
        {
            return Game.UserNumbers.Count > 0;
        }

        private void ExecuteRemoveCommand(object parameter)
        {
            if (Game.UserNumbers.Contains(SelectedNumber))
                Game.UserNumbers.Remove(SelectedUserNumber);
        }

        #endregion
        #region Play Command Methods

        private bool CanExecutePlayCommand(object parameter)
        {
            return (Game.UserNumbers.Count > 0) &&
                   (Game.UserNumbers.Count == Game.Spot) &&
                   (!string.IsNullOrEmpty(Game.PlayerName) &&
                   (!IsPlayedDirty));
        }

        private void ExecutePlayCommand(object parameter)
        {
            Game.GetNumbers();

            ScoreControl scoreControl = new ScoreControl();

            ScoreViewModel viewModel = new ScoreViewModel(Game, _scoring);
            scoreControl.DataContext = viewModel;

            scoreControl.ShowDialog();
            IsPlayedDirty = true;
        }

        #endregion

        #region Restart/Replay Command Methods

        private void ExecuteRestartCommand(object parameter)
        {
            IsPlayedDirty = false;
            _game.RandomNumbers.Clear();
            _game.UserNumbers.Clear();
        }

        #endregion
    }
}
