﻿using Keno.Model;
using Keno.ViewModel.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keno.ViewModel
{
    class OptionsDisplayViewModel : PropertyChangedBase
    {
        #region Variables

        private readonly ObservableCollection<int> _pickNumberList = PickNumberDataSource.PopulatePickNumberList();
        private readonly ObservableCollection<int> _spotNumberList = SpotNumberDataSource.PopulateSpotNumberList();
        private ObservableCollection<int> _userNumberList = UserNumberDataSource.GetUserNumbers();

        private int _selectedNumber;
        private int _selectedSpot;
        private int _selectedNumberIndex;
        private int _selectedSpotIndex;
        private string _playerName;

        #endregion
        #region Command Variables

        private CommonCommand _addCommand;
        private CommonCommand _removeCommand;

        #endregion


        #region Properties

        public string PlayerName
        {
            get
            { return _playerName; }
            set
            {
                _playerName = value;
                OnPropertyChanged(nameof(PlayerName));
            }
        }

        public int SelectedNumber
        {
            get
            { return _selectedNumber; }
            set
            {
                _selectedNumber = value;
                OnPropertyChanged(nameof(SelectedNumber));
            }
        }

        public int SelectedSpot
        {
            get
            { return _selectedSpot; }
            set
            {
                _selectedSpot = value;
                OnPropertyChanged(nameof(SelectedSpot));
            }
        }

        public int SelectedNumberIndex
        {
            get
            { return _selectedNumberIndex; }
            set
            {
                _selectedNumberIndex = value;
                OnPropertyChanged(nameof(SelectedNumber));
                OnPropertyChanged(nameof(SelectedNumberIndex));
            }
        }

        public int SelectedSpotIndex
        {
            get
            { return _selectedSpotIndex; }
            set
            {
                _selectedSpotIndex = value;
                OnPropertyChanged(nameof(SelectedSpot));
                OnPropertyChanged(nameof(SelectedSpotIndex));
            }
        }

        #endregion
        #region List Properties

        public ObservableCollection<int> ToPickNumbers
        {
            get
            {
                return _pickNumberList;
            }
        }

        public ObservableCollection<int> SpotNumbers
        {
            get
            {
                return _spotNumberList;
            }
        }

        public ObservableCollection<int> UserNumbers
        {
            get
            {
                return _userNumberList;
            }
        }

        #endregion
        #region Command Properties

        public CommonCommand AddCommand
        {
            get
            {
                return _addCommand;
            }
            set
            {
                _addCommand = value;
            }
        }

        public CommonCommand RemoveCommand
        {
            get
            {
                return _removeCommand;
            }
            set
            {
                _removeCommand = value;
            }
        }

        #endregion


        #region Constructor

        public OptionsDisplayViewModel()
        {
            _addCommand = new CommonCommand(ExecuteAddCommand, CanExecuteAddCommand);
            _removeCommand = new CommonCommand(ExecuteRemoveCommand, CanExecuteRemoveCommand);

        }

        #endregion // Constructor

        #region Add Command Methods

        public bool CanExecuteAddCommand(object parameter)
        {
            return UserNumbers.Count <= SelectedSpot;
        }

        public void ExecuteAddCommand(object parameter)
        {
            UserNumberDataSource.AddNumber(SelectedNumber);
        }

        #endregion

        #region Remove Command Methods

        public bool CanExecuteRemoveCommand(object parameter)
        {
            return UserNumbers.Count > 0;
        }

        public void ExecuteRemoveCommand(object parameter)
        {
            UserNumberDataSource.RemoveNumber(SelectedNumber);
        }

        #endregion
    }
}
