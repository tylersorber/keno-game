﻿using Keno.Model;
using Keno.Model.DataSource;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keno.ViewModel
{
    internal class RandomNumberViewModel
    {
        #region Variables

        ObservableCollection<int> _randNumbList;

        #endregion

        #region Properties

        public string NumbersToDisplay
        {
            get
            {
                return GetAllNumbersToDisplay();
            }
        }

        #endregion
        
        #region Constructor

        public RandomNumberViewModel()
        {
            _randNumbList = RandomNumberDataSource.GetRandomNumbers();
        }

        #endregion // Constructor

        #region Helper Methods

        public string GetAllNumbersToDisplay()
        {
            List<int> sorted = _randNumbList.OrderBy(x => x).ToList();

            StringBuilder display = new StringBuilder();
            string delimiter = " | ";

            for (int i = 0; i < sorted.Count; i++)
            {
                display.Append(sorted[i].ToString());
                display.Append(delimiter);
            }

            // Remove the last " | "
            display.Remove(display.Length - delimiter.Length, delimiter.Length);

            return display.ToString();
        }

        #endregion
    }
}
