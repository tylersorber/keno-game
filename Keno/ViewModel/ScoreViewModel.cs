﻿using Keno.Model;

namespace Keno.ViewModel
{
    public class ScoreViewModel
    {
        #region Variables

        private Game _game;
        private int _score;

        #endregion

        #region Properties

        public Game Game
        {
            get { return _game; }
        }

        public int Score
        {
            get { return _score; }
        }

        #endregion

        #region Constructor

        public ScoreViewModel(Game game, ScoreItemList scoring)
        {
            _game = game;

            int correct = CorrectNumbers(game);
            _score = scoring[game.Spot].ScoreValueList[correct].Score;
        }

        #endregion

        #region Methods

        private int CorrectNumbers(Game game)
        {
            int correct = 0;
            for (int i = 0; i < game.UserNumbers.Count; i++)
            {
                if (game.RandomNumbers.Contains(game.UserNumbers[i]))
                    correct++;
            }

            return correct;
        }

        #endregion
    }
}
