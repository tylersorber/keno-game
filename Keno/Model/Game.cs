﻿using Keno.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

// BitBucket change
//
//
namespace Keno.Model
{
    public class Game : PropertyChangedBase
    {
        #region Constants

        private const int TOTAL_LIST_MAX = 20;
        private const int NUMBER_MAX = 1;
        private const int NUMBER_MIN = 80;

        #endregion

        #region Variables 

        private string _playerName;
        private int _spot;
        private string _strRandNumbers = "000";
        private string _struserNumbers = "000";
        private ObservableCollection<int> _userNumbers = new ObservableCollection<int>();
        private List<int> _randNumbers = new List<int>();

        #endregion

        #region Properties

        public string PlayerName
        {
            get { return _playerName; }
            set
            {
                _playerName = value;
                OnPropertyChanged(nameof(PlayerName));
            }
        }

        public int Spot
        {
            get { return _spot; }
            set
            {
                _spot = value;
                OnPropertyChanged(nameof(Spot));
            }
        }

        public ObservableCollection<int> UserNumbers
        {
            get { return _userNumbers; }
        }

        public List<int> RandomNumbers
        {
            get
            {
                return _randNumbers;
            }
        }

        public string RandomNumbersToDisplay
        {
            get
            { return _strRandNumbers; }
            set
            {
                _strRandNumbers = value;
                OnPropertyChanged(nameof(RandomNumbersToDisplay));
            }
        }

        public string UserNumbersToDisplay
        {
            get
            { return _struserNumbers; }
            set
            {
                _struserNumbers = value;
                OnPropertyChanged(nameof(UserNumbersToDisplay));
            }
        }

        #endregion

        #region Methods

        private string ConvertListToString(List<int> list)
        {
            if (list.Count == 0)
                return "000";

            list.Sort();

            StringBuilder display = new StringBuilder();
            string delimiter = " | ";

            for (int i = 0; i < list.Count; i++)
            {
                display.Append(list[i].ToString());
                display.Append(delimiter);
            }

            // Remove the last " | "
            display.Remove(display.Length - delimiter.Length, delimiter.Length);

            return display.ToString();
        }

        public void GetNumbers()
        {
            int counter = 0;

            while (counter < TOTAL_LIST_MAX)
            {
                Random random = new Random();
                int randNumber = random.Next(NUMBER_MAX, NUMBER_MIN + 1);

                if (!RandomNumbers.Contains(randNumber))
                {
                    RandomNumbers.Add(randNumber);
                    counter++;
                }
            }

            RandomNumbersToDisplay = ConvertListToString(RandomNumbers);
            UserNumbersToDisplay = ConvertListToString(UserNumbers.ToList<int>());
        }

        #endregion
    }
}
