﻿using System.Collections.ObjectModel;

namespace Keno.Model
{
    public static class SpotNumberDataSource
    {
        #region Constants

        private static int NUMBERS_TO_PLAY_MAX = 10;
        private static int NUMBERS_TO_PLAY_MIN = 1;

        #endregion

        #region Variables

        private static ObservableCollection<int> _spotNumbers = new ObservableCollection<int>();

        #endregion

        #region Methods

        public static ObservableCollection<int> PopulateSpotNumberList()
        {
            if (_spotNumbers.Count == 0)
            {
                for (int i = NUMBERS_TO_PLAY_MIN; i <= NUMBERS_TO_PLAY_MAX; i++)
                {
                    _spotNumbers.Add(i);
                }
            }

            return _spotNumbers;
        }

        #endregion
    }
}
