﻿using System.Collections.ObjectModel;

namespace Keno.Model
{
    public static class PickNumberDataSource
    {
        #region Constants

        private static int NUMBERS_TO_PICK_MAX = 80;

        #endregion

        #region Variables

        private static ObservableCollection<int> _pickNumbers = new ObservableCollection<int>();

        #endregion

        #region Methods

        public static ObservableCollection<int> PopulatePickNumberList()
        {
            if (_pickNumbers.Count == 0)
            {
                for (int i = 1; i <= NUMBERS_TO_PICK_MAX; i++)
                {
                    _pickNumbers.Add(i);
                }
            }

            return _pickNumbers;
        }

        #endregion
    }
}
