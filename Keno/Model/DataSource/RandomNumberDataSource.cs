﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keno.Model.DataSource
{
    public static class RandomNumberDataSource
    {
        #region Constants

        private const int TOTAL_LIST_MAX = 20;
        private const int NUMBER_MAX = 1;
        private const int NUMBER_MIN = 80;

        #endregion

        #region Variables

        private static ObservableCollection<int> _randomNumbers = new ObservableCollection<int>();

        #endregion

        #region Methods

        public static ObservableCollection<int> GetRandomNumbers()
        {
            int counter = 0;

            while (counter < TOTAL_LIST_MAX)
            {
                Random random = new Random();
                int randNumber = random.Next(NUMBER_MAX, NUMBER_MIN + 1);

                if (!_randomNumbers.Contains(randNumber))
                {
                    _randomNumbers.Add(randNumber);
                    counter++;
                }
            }

            return _randomNumbers;
        }

        #endregion
    }
}
