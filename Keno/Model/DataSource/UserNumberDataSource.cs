﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keno.Model
{
    public static class UserNumberDataSource
    {
        #region Constants

        #endregion

        #region Variables

        private static ObservableCollection<int> _userNumbers = new ObservableCollection<int>();

        #endregion

        #region Methods

        public static ObservableCollection<int> GetUserNumbers()
        {
            return _userNumbers;
        }

        public static void AddNumber(int value)
        {
            if (!_userNumbers.Contains(value))
                _userNumbers.Add(value);
        }

        public static void RemoveNumber(int value)
        {
            if (_userNumbers.Contains(value))
                _userNumbers.Remove(value);
        }

        #endregion
    }
}
