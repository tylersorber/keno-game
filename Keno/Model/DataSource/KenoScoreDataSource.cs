﻿using System.Collections.Generic;

namespace Keno.Model
{
    public static class KenoScoreDataSource
    {
        #region Variables

        private static List<KeyValuePair<int, int>> _kenoNumbers = new List<KeyValuePair<int, int>>();

        #endregion

        #region Methods

        public static List<KeyValuePair<int, int>> PopulateScoreChart(int spot)
        {
            if (_kenoNumbers.Count > 0)
                return _kenoNumbers;

            switch (spot)
            {
                case 1:
                    _kenoNumbers.Add(new KeyValuePair<int, int>(0, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(1, 2));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(2, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(3, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(4, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(5, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(6, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(7, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(8, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(9, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(10, 0));

                    break;
                case 2:
                    _kenoNumbers.Add(new KeyValuePair<int, int>(0, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(1, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(2, 11));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(3, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(4, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(5, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(6, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(7, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(8, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(9, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(10, 0));

                    break;
                case 3:
                    _kenoNumbers.Add(new KeyValuePair<int, int>(0, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(1, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(2, 2));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(3, 27));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(4, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(5, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(6, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(7, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(8, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(9, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(10, 0));

                    break;
                case 4:
                    _kenoNumbers.Add(new KeyValuePair<int, int>(0, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(1, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(2, 1));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(3, 5));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(4, 72));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(5, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(6, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(7, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(8, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(9, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(10, 0));

                    break;
                case 5:
                    _kenoNumbers.Add(new KeyValuePair<int, int>(0, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(1, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(2, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(3, 2));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(4, 18));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(5, 410));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(6, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(7, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(8, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(9, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(10, 0));

                    break;
                case 6:
                    _kenoNumbers.Add(new KeyValuePair<int, int>(0, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(1, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(2, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(3, 1));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(4, 7));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(5, 57));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(6, 1100));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(7, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(8, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(9, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(10, 0));

                    break;
                case 7:
                    _kenoNumbers.Add(new KeyValuePair<int, int>(0, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(1, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(2, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(3, 1));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(4, 5));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(5, 11));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(6, 100));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(7, 2000));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(8, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(9, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(10, 0));

                    break;
                case 8:
                    _kenoNumbers.Add(new KeyValuePair<int, int>(0, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(1, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(2, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(3, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(4, 2));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(5, 15));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(6, 50));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(7, 300));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(8, 10000));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(9, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(10, 0));

                    break;
                case 9:
                    _kenoNumbers.Add(new KeyValuePair<int, int>(0, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(1, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(2, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(3, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(4, 2));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(5, 5));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(6, 20));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(7, 100));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(8, 2000));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(9, 25000));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(10, 0));

                    break;
                case 10:
                    _kenoNumbers.Add(new KeyValuePair<int, int>(0, 5));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(1, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(2, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(3, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(4, 0));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(5, 2));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(6, 10));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(7, 50));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(8, 500));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(9, 5000));
                    _kenoNumbers.Add(new KeyValuePair<int, int>(10, 100000));

                    break;

            }

            return _kenoNumbers;
        }

        #endregion
    }

}
