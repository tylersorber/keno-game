﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keno.Model
{
    public class RandomNumber : IComparable
    {
        #region Constants

        private int NUMBER_MAX = 1;
        private int NUMBER_MIN = 80;

        #endregion

        #region Variables 

        private int _randomNumber;

        #endregion

        #region Properties

        public int Value
        {
            get
            {
                return _randomNumber;
            }
        }

        #endregion

        #region Constructor

        public RandomNumber()
        {
            _randomNumber = GetRandomNumber();
        }


        #endregion

        #region Helper Methods

        private int GetRandomNumber()
        {
            Random random = new Random();

            return random.Next(NUMBER_MAX, NUMBER_MIN + 1);
        }


        #endregion

        #region IComparable Implementation

        public int CompareTo(object obj)
        {
            RandomNumber randNumb = (RandomNumber)obj;

            int cmpl = this.Value.CompareTo(randNumb.Value);

            if (!(cmpl == 0))
                return cmpl;

            return this.Value.CompareTo(randNumb.Value);
        }

        #endregion
    }

    public class RandomNumberList : CollectionBase
    {
        #region Constants

        private const int TOTAL_LIST_MAX = 20;

        #endregion

        #region Indexer

        public RandomNumber this[int idx]
        {
            get
            {
                return (RandomNumber)this.InnerList[idx];
            }
        }

        #endregion

        #region Constructor 

        public RandomNumberList()
        {
            PopulateRandomNumberList();
        }

        #endregion

        #region Helper Methods

        private void PopulateRandomNumberList()
        {
            int counter = 0;

            while (counter < TOTAL_LIST_MAX)
            {
                RandomNumber randNumber = new RandomNumber();

                if (!this.Contains(randNumber))
                {
                    this.Add(randNumber);
                    counter++;
                }
            }
        }

        #endregion

        #region CollectionBase Implementation Methods

        public void Add(RandomNumber randomNumber)
        {
            this.InnerList.Add(randomNumber);
        }

        public void Remove(RandomNumber randomNumber)
        {
            if (!this.Contains(randomNumber)) return;

            int i;
            for (i = 0; i <= this.InnerList.Count; i++)
            {
                RandomNumber item = (RandomNumber)this.InnerList[i];
                if ((item.CompareTo(randomNumber) == 0))
                {
                    this.RemoveAt(i);
                    return;
                }
            }
        }

        public bool Contains(RandomNumber randomNumber)
        {
            foreach (RandomNumber item in this.InnerList)
            {
                if (item.CompareTo(randomNumber) == 0)
                    return true;
            }

            return false;
        }

        public RandomNumber[] ToArray()
        {
            return (RandomNumber[])this.InnerList.ToArray(typeof(RandomNumber));
        }

        public int IndexOf(RandomNumber randNumber)
        {
            return this.InnerList.IndexOf(randNumber);
        }
        #endregion
    }
}