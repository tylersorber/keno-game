﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Keno.Model
{
    public class ScoreItem : IComparable
    {
        #region Variables

        private int _spot;
        private readonly ScoreValueList _scoreValueList;

        #endregion

        #region Properties

        public int Spot
        {
            get { return _spot; }
        }

        public ScoreValueList ScoreValueList
        {
            get { return _scoreValueList; }
        }

        #endregion

        #region Constructors

        public ScoreItem(int spot, ScoreValueList list)
        {
            _spot = spot;
            _scoreValueList = list;
        }

        #endregion

        #region IComparable Implementation

        public int CompareTo(object obj)
        {
            ScoreItem item = (ScoreItem)obj;
            return Spot.CompareTo(item.Spot);
        }

        #endregion
    }

    public class ScoreItemList : CollectionBase
    {
        #region Indexer

        public ScoreItem this[int spot]
        {
            get
            {
                return Find(spot);
            }

        }

        #endregion

        #region Constructors

        public ScoreItemList(int possibleSpots)
        {
            for (int i = 0; i < possibleSpots; i++)
            {
                int spot = i + 1;

                List<KeyValuePair<int, int>> source = KenoScoreDataSource.PopulateScoreChart(spot);

                ScoreValueList valueList = new ScoreValueList();
                for (int x = 0; x < source.Count; x++)
                {
                    valueList.Add(new ScoreValue(source[x].Key, source[x].Value));
                }

                ScoreItem item = new ScoreItem(spot, valueList);
                Add(item);
            }
        }

        #endregion

        #region Methods

        public List<ScoreItem> ToList()
        {
            return (List<ScoreItem>)List;
        }

        private ScoreItem Find(int spot)
        {
            for (int i = 0; i < List.Count; i++)
            {
                ScoreItem item = (ScoreItem)List[i];

                if (item.Spot == spot)
                    return item;
            }

            return null;
        }

        #endregion

        #region CollectionBase Implementation

        public void Add(ScoreItem scoreItem)
        {
            List.Add(scoreItem);
        }

        public void Remove(ScoreItem scoreItem)
        {
            if (!this.Contains(scoreItem))
                return;

            for (int i = 0; i <= List.Count; i++)
            {
                ScoreItem item = (ScoreItem)List[i];
                if ((item.CompareTo(scoreItem) == 0))
                {
                    this.RemoveAt(i);
                    return;
                }
            }
        }

        public bool Contains(ScoreItem scoreItem)
        {
            foreach (ScoreItem item in List)
            {
                if (item.CompareTo(scoreItem) == 0)
                {
                    return true;
                }
            }

            return false;
        }


        #endregion
    }
}
