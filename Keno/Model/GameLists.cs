﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keno.Model
{
    public static class GameLists
    {
        public static ObservableCollection<int> PossibleNumbers()
        {
            return PickNumberDataSource.PopulatePickNumberList();
        }

        public static ObservableCollection<int> PossileSpots()
        {
            return SpotNumberDataSource.PopulateSpotNumberList();
        }
    }
}
