﻿using Keno.ViewModel.Commands;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Keno.Model
{
    public class UserNumber : IComparable
    {
        #region Variables 

        private int _userNumber;

        #endregion

        #region Properties

        public int Value
        {
            get
            {
                return _userNumber;
            }
        }

        #endregion

        #region Constructor

        public UserNumber(int value)
        {
            _userNumber = value;
        }


        #endregion

        #region IComparable Implementation

        public int CompareTo(object obj)
        {
            UserNumber randNumb = (UserNumber)obj;

            int cmpl = this.Value.CompareTo(randNumb.Value);

            if (!(cmpl == 0))
                return cmpl;

            return this.Value.CompareTo(randNumb.Value);
        }

        #endregion
    }

    public class UserNumberList : CollectionBase
    {
        #region Varaibles

        private int _numberOfSpots = 0;

        #endregion

        #region Indexer

        public UserNumber this[int idx]
        {
            get
            {
                return (UserNumber)this.InnerList[idx];
            }
        }

        #endregion

        #region Constructor 

        public UserNumberList(int spots)
        {
            _numberOfSpots = spots;

            AddNumberCommand = new AddCommand();
        }

        #endregion

        #region CollectionBase Implementation Methods

        public void Add(UserNumber UserNumber)
        {
            this.InnerList.Add(UserNumber);
        }

        public void Remove(UserNumber UserNumber)
        {
            if (!this.Contains(UserNumber)) return;

            int i;
            for (i = 0; i <= this.InnerList.Count; i++)
            {
                UserNumber item = (UserNumber)this.InnerList[i];
                if ((item.CompareTo(UserNumber) == 0))
                {
                    this.RemoveAt(i);
                    return;
                }
            }
        }

        public bool Contains(UserNumber UserNumber)
        {
            foreach (UserNumber item in this.InnerList)
            {
                if (item.CompareTo(UserNumber) == 0)
                    return true;
            }

            return false;
        }

        public UserNumber[] ToArray()
        {
            return (UserNumber[])this.InnerList.ToArray(typeof(UserNumber));
        }

        public int IndexOf(UserNumber randNumber)
        {
            return this.InnerList.IndexOf(randNumber);
        }
        #endregion

        #region ICommand Helper Properties

        public ICommand AddNumberCommand { get; set; }

        #endregion
    }
}