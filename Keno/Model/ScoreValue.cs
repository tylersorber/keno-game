﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Keno.Model
{
    public class ScoreValue : IComparable
    {
        #region Variables

        private int _correct;
        private int _score;

        #endregion

        #region Properties

        public int Correct
        {
            get { return _correct; }
        }

        public int Score
        {
            get { return _score; }
        }

        #endregion

        #region Constructors

        public ScoreValue(int correct, int score)
        {
            _correct = correct;
            _score = score;
        }

        #endregion

        #region IComparable Implementation

        public int CompareTo(object obj)
        {
            ScoreValue item = (ScoreValue)obj;
            return Correct.CompareTo(item.Correct);
        }

        #endregion
    }

    public class ScoreValueList : CollectionBase
    {
        #region Indexer

        public ScoreValue this[int correct]
        {
            get
            {
                return Find(correct);
            }

        }

        #endregion

        #region Constructors
        public ScoreValueList()
        {


        }
        #endregion

        #region Methods

        private ScoreValue Find(int correct)
        {
            for (int i = 0; i < List.Count; i++)
            {
                ScoreValue item = (ScoreValue)List[i];

                if (item.Correct == correct)
                    return item;
            }

            return null;
        }

        public List<ScoreValue> ToList()
        {
            return (List<ScoreValue>)List;
        }

        #endregion

        #region CollectionBase Implementation

        public void Add(ScoreValue scoreValue)
        {
            List.Add(scoreValue);
        }

        public void Remove(ScoreValue scoreValue)
        {
            if (!this.Contains(scoreValue))
                return;

            for (int i = 0; i <= List.Count; i++)
            {
                ScoreValue item = (ScoreValue)List[i];
                if ((item.CompareTo(scoreValue) == 0))
                {
                    this.RemoveAt(i);
                    return;
                }
            }
        }

        public bool Contains(ScoreValue scoreValue)
        {
            foreach (ScoreValue item in List)
            {
                if (item.CompareTo(scoreValue) == 0)
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
