﻿using Keno.ViewModel;
using System.Windows;

namespace Keno.UserControls
{
    public partial class GameControl : Window
    {
        #region Constructor

        public GameControl()
        {
            InitializeComponent();
            DataContext = new GameViewModel();
        }

        #endregion
    }
}
